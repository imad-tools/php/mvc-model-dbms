<?php

namespace src\model;

use JsonSerializable;
use PDOException;
use PDOStatement;

/**
 * Model base class
 */
class AbstractModel {

    /**
     * @var string $tableName
     */
    protected static $tableName;

    /**
     * @var string $entityClass
     */
    protected static $entityClass;

    /**
     * @var string $primaryKey
     */
    protected static $primaryKey;

    /**
     * @var Database $database
     */
    protected static $database;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        if (static::$database == NULL) {
            static::$database = new Database();
        }
    }

    /**
     * findOneBy()
     *
     * @param array $proprieties which used to identify a record with AND logic
     * @return mixed|null
     * @throws PDOException when the $proprieties are incorrect
     */
    public function findOneBy(array $proprieties = []) {
        $sql = 'SELECT * FROM ' . static::$tableName;
        if (count($proprieties)) {
            $sql .= ' WHERE ';
        }

        foreach ($proprieties as $propriety => $value) {
            $sql .= $propriety . " = '$value'";
            if (array_key_last($proprieties) != $propriety) {
                $sql .= ' AND ';
            }
        }

        $sql .= ' LIMIT 1;';

        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->query($sql);

        if ($statments) {
            $entity = $statments->fetchObject(static::$entityClass);
            if ($entity)
                return $entity;
            return NULL;
        } else {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }
    }

    /**
     * findBy()
     *
     * @param array $proprieties which used to identify a record with AND logic
     * @return array
     * @throws PDOException when the $proprieties or $orderBy are incorrect
     */
    public function findBy(array $proprieties, array $orderBy = null, $limit = null, $offset = null) {
        $sql = 'SELECT * FROM ' . static::$tableName;

        // where
        if (count($proprieties)) {
            $sql .= ' WHERE ';
            foreach ($proprieties as $propriety => $value) {
                $sql .= $propriety . " = '$value'";
                if (array_key_last($proprieties) != $propriety) {
                    $sql .= ' AND ';
                }
            }
        }

        // order by
        if ($orderBy !== null and count($orderBy)) {
            $sql .= ' ORDER BY ';
            foreach ($orderBy as $propriety => $value) {
                $sql .= $propriety . " $value";
                if (array_key_last($orderBy) != $propriety) {
                    $sql .= ', ';
                }
            }
        }

        // limit
        if ($limit !== null) {
            $sql .= ' LIMIT ' . $limit;
        }

        // offset
        if ($offset !== null) {
            $sql .= ' OFFSET ' . $offset;
        }

        // end
        $sql .= ';';

        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->query($sql);

        if ($statments) {
            return $statments->fetchAll(Database::FETCH_CLASS, static::$entityClass);
        } else {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }
    }

    /**
     * findBy()
     *
     * @return array
     */
    public function findAll() {
        $sql = 'SELECT * FROM ' . static::$tableName;

        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->query($sql);

        if (!$statments) {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }

        return $statments->fetchAll(Database::FETCH_CLASS, static::$entityClass);
    }

    /**
     * @param $primaryValue
     * @return mixed|null
     * @throws PDOException
     */
    public function find($primaryValue) {
        $sql = 'SELECT * FROM ' . static::$tableName . ' WHERE ' . static::$primaryKey . ' = :' . static::$primaryKey . ';';

        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->prepare($sql);

        if (!$statments) {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }

        $statments->bindValue(static::$primaryKey, $primaryValue, Database::PARAM_INT);
        $statments->execute();

        $entity = $statments->fetchObject(static::$entityClass);
        if ($entity)
            return $entity;
        return NULL;
    }

    /**
     * @param JsonSerializable $entity
     * @return mixed|null
     * @throws PDOException
     */
    public function insert($entity) {
        $sql = 'INSERT INTO ' . static::$tableName;

        /**
         * columns
         * @var array $columns
         */
        $columns = $entity->jsonSerialize();
        $camps = ' (';
        $values = ' VALUES (';
        foreach ($columns as $key => $value)  {
            if ($key != static::$primaryKey) {
                $camps .= $key;
                $values .= ":$key";
                if (array_key_last($columns) != $key) {
                    $camps .= ', ';
                    $values .= ', ';
                }
            }
        }
        $camps .= ')';
        $values .= ')';

        // prepare query
        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->prepare($sql . $camps . $values);

        // check if query is valid
        if (!$statments) {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }

        // bind params
        foreach ($columns as $key => $value)  {
            $statments->bindValue($key, $value);
        }

        // execute query
        $statments->execute();
    }

    /**
     * @param $entity
     * @return mixed|null
     * @throws PDOException
     */
    public function update(JsonSerializable $entity) {
        $sql = 'UPDATE ' . static::$tableName;

        /**
         * columns
         * @var array $columns
         */
        $columns = $entity->jsonSerialize();

        // set
        $sql .= ' SET ';
        foreach ($columns as $key => $value)  {
            if ($key != static::$primaryKey) {
                $sql .= $key . ' = :' . $key;
                if (array_key_last($columns) != $key) {
                    $sql .= ', ';
                }
            }
        }

        // where
        $sql .= ' WHERE ' . static::$primaryKey . ' = :' . static::$primaryKey;

        // prepare query
        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->prepare($sql);

        // check if query is valid
        if (!$statments) {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }

        // bind params
        foreach ($columns as $key => $value)  {
            $statments->bindValue($key, $value);
        }

        // execute query
        $statments->execute();
    }

    /**
     * @param $entity
     * @return mixed|null
     * @throws PDOException
     */
    public function delete(JsonSerializable $entity) {
        // delete query
        $sql = 'DELETE FROM ' . static::$tableName;

        // where
        $sql .= ' WHERE ' . static::$primaryKey . ' = :' . static::$primaryKey;

        // prepare query
        /**
         * @var PDOStatement $statments
         */
        $statments = static::$database->prepare($sql);

        // check if query is valid
        if (!$statments) {
            throw new PDOException(static::$database->errorInfo()[2], static::$database->errorInfo()[1]);
        }

        /**
         * columns
         * @var array $columns
         */
        $columns = $entity->jsonSerialize();

        // bind
        $statments->bindValue(static::$primaryKey, $columns[static::$primaryKey], Database::PARAM_INT);

        // execute
        $statments->execute();
    }
}
